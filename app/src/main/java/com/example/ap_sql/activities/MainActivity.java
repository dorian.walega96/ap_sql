package com.example.ap_sql.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityMainBinding;
import com.example.ap_sql.model.Post;
import com.example.ap_sql.model.PostViewModel;
import com.example.ap_sql.model.Reaction;
import com.example.ap_sql.utils.DBHelper;
import com.example.ap_sql.utils.PostRecyclerViewAdapter;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends BaseActivity {

    private static int ADD_POST = 0;

    ActivityMainBinding binding;
    private PostRecyclerViewAdapter postRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        String login = getIntent().getExtras().getString("login");
        user = dbHelper.getUser(login);
        setupRecyclerView();
        binding.btnAdd.setOnClickListener(view -> startActivityForResult(new Intent(this, AddActivity.class), ADD_POST));
        binding.btnTest.setOnClickListener(view -> startActivity(new Intent(this, TestActivity.class)));
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = binding.postRecyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        postRecyclerViewAdapter = new PostRecyclerViewAdapter(downloadPosts());
        recyclerView.setAdapter(postRecyclerViewAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ADD_POST) {
            refreshPosts();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void refreshPosts() {
        postRecyclerViewAdapter.setPostList(downloadPosts());
        postRecyclerViewAdapter.notifyDataSetChanged();
    }

    private ArrayList<PostViewModel> downloadPosts() {
        ArrayList<Post> posts = dbHelper.getPosts();
        ArrayList<PostViewModel> postModels = new ArrayList<>();
        posts.forEach(post -> {
            ArrayList<Reaction> reactions = dbHelper.getReactionsForPost(post.getId());
            postModels.add(new PostViewModel(post, reactions));
        });
        return postModels;
    }

    public void reaction(PostViewModel model) {
        dbHelper.postReact(model.getPost(), user);
        refreshPosts();
    }

    public void showPost(PostViewModel model) {
        Intent intent = new Intent(this, PostActivity.class);
        Bundle bundle = new Bundle(1);
        bundle.putInt("postId", model.getPost().getId());
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
