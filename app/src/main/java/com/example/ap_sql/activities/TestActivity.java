package com.example.ap_sql.activities;

import android.os.Bundle;
import android.util.Log;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityTestBinding;
import com.example.ap_sql.utils.TestBench;
import com.example.ap_sql.utils.TimeService;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class TestActivity extends BaseActivity {

    private TestBench testBench;
    ActivityTestBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        testBench = new TestBench(getApplicationContext());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test);
        binding.run.setOnClickListener(view -> runTests(100));
    }

    public void runTests(int count){
        TimeService.startTimer();
        testBench.testInsert(count);
        long insertResult = TimeService.stopTimer();
        Log.v("MAG_RESULT", "INSERT, " + count + ", " + insertResult);

        TimeService.startTimer();
        testBench.testSelect(count);
        long selectResult = TimeService.stopTimer();
        Log.v("MAG_RESULT", "SELECT, " + count + ", " + selectResult);

        TimeService.startTimer();
        testBench.testDelete(count);
        long deleteResult = TimeService.stopTimer();
        Log.v("MAG_RESULT", "DELETE, " + count + ", " + deleteResult);
    }
}
