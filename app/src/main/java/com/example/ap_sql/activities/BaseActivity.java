package com.example.ap_sql.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ap_sql.model.User;
import com.example.ap_sql.utils.DBHelper;

public class BaseActivity extends AppCompatActivity {

    protected static User user;
    protected static DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(getApplicationContext());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        BaseActivity.user = user;
    }

    public static DBHelper getDbHelper() {
        return dbHelper;
    }

    public static void setDbHelper(DBHelper dbHelper) {
        BaseActivity.dbHelper = dbHelper;
    }
}
