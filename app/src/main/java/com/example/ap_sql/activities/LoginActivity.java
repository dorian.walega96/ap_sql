package com.example.ap_sql.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityLoginBinding;


public class LoginActivity extends BaseActivity {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setBindings();
    }

    public void setBindings() {
        binding.btnRegister.setOnClickListener(view -> startActivity(new Intent(this, RegisterActivity.class)));
        binding.btnLogin.setOnClickListener(view -> {
            if (loginUser(binding.getLogin(), binding.getPassword())) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("login", binding.getLogin());
                startActivity(intent);
            } else {
                Toast.makeText(this, "Wrong login or password!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean loginUser(String login, String password) {
        return dbHelper.login(login, password);
    }
}
