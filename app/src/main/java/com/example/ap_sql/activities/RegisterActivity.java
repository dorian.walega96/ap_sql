package com.example.ap_sql.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityRegisterBinding;
import com.example.ap_sql.model.User;
import com.example.ap_sql.utils.DBHelper;

import java.util.ArrayList;


public class RegisterActivity extends BaseActivity {

    ActivityRegisterBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        setBindings();
    }

    private void setBindings() {
        binding.setNewUser(new User());
        binding.setPassCheck("");
        binding.btnRegister.setOnClickListener(view -> {
            long result = registerNewUser();
            if (result > -1) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("login", binding.getNewUser().getLogin());
                startActivity(intent);
            }
        });
    }

    private long registerNewUser() {
        if (validateNewUser(binding.getNewUser())) {
            return dbHelper.register(binding.getNewUser());
        }
        return -1;
    }

    private boolean validateNewUser(User user) {
        ArrayList<User> users = new ArrayList<>();
        if (!users.isEmpty()) {
            Toast.makeText(this, "User already exists!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!user.getPassword().equals(binding.getPassCheck())) {
            Toast.makeText(this, "Password do not match!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (user.getPassword().length() < 4) {
            Toast.makeText(this, "Password too short!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
