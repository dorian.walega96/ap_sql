package com.example.ap_sql.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityAddBinding;
import com.example.ap_sql.model.Post;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;


public class AddActivity extends BaseActivity {

    ActivityAddBinding binding;
    private LocationManager locationManager;
    private Geocoder geocoder;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add);
        binding.btnAdd.setOnClickListener(view -> addPost());
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
    }

    private void addPost() {
        Post post = new Post();
        post.setAuthor(user);
        post.setDate(new Date());
        post.setText(binding.getText());
        post.setLocation(prepareLocation());
        dbHelper.addPost(post);
        finish();
    }

    private String prepareLocation() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                try {
                    List<Address> addressList = geocoder.getFromLocation(locationGPS.getLatitude(), locationGPS.getLongitude(), 1);
                    if (addressList.size() > 0)
                        return addressList.get(0).getLocality();
                    else
                        return null;
                } catch (IOException e) {
                    return null;
                }
            }
            return null;
        }
    }
}
