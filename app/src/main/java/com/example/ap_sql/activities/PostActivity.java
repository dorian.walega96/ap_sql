package com.example.ap_sql.activities;

import android.os.Bundle;

import com.example.ap_sql.R;
import com.example.ap_sql.databinding.ActivityPostBinding;
import com.example.ap_sql.model.Comment;
import com.example.ap_sql.model.CommentViewModel;
import com.example.ap_sql.model.Post;
import com.example.ap_sql.model.PostViewModel;
import com.example.ap_sql.model.Reaction;
import com.example.ap_sql.utils.CommentRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PostActivity extends BaseActivity {

    ActivityPostBinding binding;
    private Post post;
    private CommentRecyclerViewAdapter commentRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post);
        post = dbHelper.getPost(getIntent().getExtras().getInt("postId"));
        ArrayList<Reaction> reactions = dbHelper.getReactionsForPost(post.getId());
        binding.setPost(new PostViewModel(post, reactions));
        setupRecyclerView();
        binding.addBtn.setOnClickListener(view -> addComment());
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = binding.commentRecyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        commentRecyclerViewAdapter = new CommentRecyclerViewAdapter(downloadComments());
        recyclerView.setAdapter(commentRecyclerViewAdapter);
    }

    private ArrayList<CommentViewModel> downloadComments() {
        ArrayList<Comment> comments = dbHelper.getComments(post.getId());
        ArrayList<CommentViewModel> commentModels = new ArrayList<>();
        comments.forEach(comment -> commentModels.add(new CommentViewModel(comment, new ArrayList<>())));
        return commentModels;
    }

    private void addComment() {
        if (binding.add.getText() != null && !binding.add.getText().toString().isEmpty()) {
            Comment comment = new Comment();
            comment.setDate(new Date());
            comment.setAuthor(user);
            comment.setPost(post);
            comment.setText(binding.add.getText().toString());
            dbHelper.addComment(comment);

            binding.add.setText("");
            commentRecyclerViewAdapter.getCommentList().add(new CommentViewModel(comment, new ArrayList<>()));
            commentRecyclerViewAdapter.notifyDataSetChanged();
            binding.commentRecyclerView.smoothScrollToPosition(commentRecyclerViewAdapter.getItemCount());
        }
    }

}
