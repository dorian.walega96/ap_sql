package com.example.ap_sql.model;

public class ReactionContract {

    private ReactionContract() {}

    public static class ReactionEntry {
        public static String TABLE_NAME = "reaction";
        public static String COLUMN_NAME_ID = "id";
        public static String COLUMN_NAME_POST_ID = "post_id";
        public static String COLUMN_NAME_AUTHOR_ID = "author_id";

        public static final String SQL_CREATE_REACTION =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_POST_ID + " INTEGER," +
                        COLUMN_NAME_AUTHOR_ID + " INTEGER)";

        public static final String SQL_DELETE_REACTION =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
