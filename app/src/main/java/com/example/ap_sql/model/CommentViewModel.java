package com.example.ap_sql.model;

import java.util.ArrayList;

public class CommentViewModel {

    private Comment comment;
    private ArrayList<CommentReaction> reactions;

    public CommentViewModel(Comment comment, ArrayList<CommentReaction> reactions) {
        this.comment = comment;
        this.reactions = reactions;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public ArrayList<CommentReaction> getReactions() {
        return reactions;
    }

    public void setReactions(ArrayList<CommentReaction> reactions) {
        this.reactions = reactions;
    }
}
