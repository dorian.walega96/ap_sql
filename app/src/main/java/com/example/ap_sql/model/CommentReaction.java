package com.example.ap_sql.model;

public class CommentReaction {

    private int id;
    private int commentId;
    private int authorId;

    public CommentReaction() {}

    public CommentReaction(int id, int commentId, int authorId) {
        this.id = id;
        this.commentId = commentId;
        this.authorId = authorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
