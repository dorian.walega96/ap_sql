package com.example.ap_sql.model;

public class CommentReactionContract {

    private CommentReactionContract() {}

    public static class CommentReactionEntry {
        public static String TABLE_NAME = "comment_reaction";
        public static String COLUMN_NAME_ID = "id";
        public static String COLUMN_NAME_COMMENT_ID = "comment_id";
        public static String COLUMN_NAME_AUTHOR_ID = "author_id";

        public static final String SQL_CREATE_COMMENT_REACTION =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_COMMENT_ID + " INTEGER," +
                        COLUMN_NAME_AUTHOR_ID + " INTEGER)";

        public static final String SQL_DELETE_COMMENT_REACTION =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
