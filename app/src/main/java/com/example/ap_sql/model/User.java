package com.example.ap_sql.model;


public class User  {

    private int id;
    private String login;
    private String name;
    private String nickname;
    private String password;

    public User() {}

    public User(int id, String login, String name, String nickname, String password) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.nickname = nickname;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
