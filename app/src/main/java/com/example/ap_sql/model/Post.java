package com.example.ap_sql.model;

import java.util.Date;

public class Post {

    private int id;
    private String text;
    private User author;
    private Date date;
    private String location;

    public Post() {}

    public Post(int id, String text, User author, Date date, String location) {
        this.id = id;
        this.text = text;
        this.author = author;
        this.date = date;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
