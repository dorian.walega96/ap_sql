package com.example.ap_sql.model;

import java.util.ArrayList;

public class PostViewModel {

    private Post post;
    private ArrayList<Reaction> reactions;

    public PostViewModel(Post post, ArrayList<Reaction> reactions) {
        this.post = post;
        this.reactions = reactions;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public ArrayList<Reaction> getReactions() {
        return reactions;
    }

    public void setReactions(ArrayList<Reaction> reactions) {
        this.reactions = reactions;
    }
}
