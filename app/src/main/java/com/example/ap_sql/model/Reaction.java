package com.example.ap_sql.model;



public class Reaction {

    private int id;
    private int postId;
    private int authorId;

    public Reaction() {}

    public Reaction(int id, int postId, int authorId) {
        this.id = id;
        this.postId = postId;
        this.authorId = authorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
