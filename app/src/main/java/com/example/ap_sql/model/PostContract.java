package com.example.ap_sql.model;

public class PostContract {

    private PostContract() {}

    public static class PostEntry {
        public static String TABLE_NAME = "post";
        public static String COLUMN_NAME_ID = "id";
        public static String COLUMN_NAME_TEXT = "text";
        public static String COLUMN_NAME_AUTHOR_ID= "author_id";
        public static String COLUMN_NAME_DATE = "date";
        public static String COLUMN_NAME_LOCATION = "location";

        public static final String SQL_CREATE_POST =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_NAME_TEXT + " TEXT, " +
                        COLUMN_NAME_AUTHOR_ID + " INTEGER, " +
                        COLUMN_NAME_DATE + " DATE, " +
                        COLUMN_NAME_LOCATION + " TEXT, " +
                        " FOREIGN KEY(" + COLUMN_NAME_AUTHOR_ID + ") REFERENCES " +
                        UserContract.UserEntry.TABLE_NAME + "(" + UserContract.UserEntry.COLUMN_NAME_ID + "));";

        public static final String SQL_DELETE_POST =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
