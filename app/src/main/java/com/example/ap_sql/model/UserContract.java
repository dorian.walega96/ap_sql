package com.example.ap_sql.model;

import android.provider.BaseColumns;

public class UserContract {

    private UserContract() {}

    public static class UserEntry implements BaseColumns {
        public static String TABLE_NAME = "user";
        public static String COLUMN_NAME_ID = "id";
        public static String COLUMN_NAME_LOGIN = "login";
        public static String COLUMN_NAME_NAME = "name";
        public static String COLUMN_NAME_NICKNAME = "nickname";
        public static String COLUMN_NAME_PASSWORD = "password";


        public static final String SQL_CREATE_USER =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_LOGIN + " TEXT," +
                        COLUMN_NAME_NAME + " TEXT," +
                        COLUMN_NAME_NICKNAME + " TEXT," +
                        COLUMN_NAME_PASSWORD + " TEXT)";

        public static final String SQL_DELETE_USER =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
