package com.example.ap_sql.model;

import java.util.Date;

public class Comment {

    private int id;
    private Post post;
    private String text;
    private User author;
    private Date date;

    public Comment() {}

    public Comment(int id, Post post, String text, User author, Date date) {
        this.id = id;
        this.post = post;
        this.text = text;
        this.author = author;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
