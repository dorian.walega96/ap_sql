package com.example.ap_sql.model;

import android.provider.BaseColumns;

public class CommentContract {

    private CommentContract() {}

    public static class CommentEntry implements BaseColumns {
        public static String TABLE_NAME = "comment";
        public static String COLUMN_NAME_ID = "id";
        public static String COLUMN_NAME_POST_ID = "post_id";
        public static String COLUMN_NAME_TEXT = "text";
        public static String COLUMN_NAME_AUTHOR_ID= "author_id";
        public static String COLUMN_NAME_DATE = "date";

        public static final String SQL_CREATE_COMMENT =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_POST_ID + " INTEGER," +
                        COLUMN_NAME_TEXT + " TEXT," +
                        COLUMN_NAME_AUTHOR_ID + " INTEGER," +
                        COLUMN_NAME_DATE + " DATE)";

        public static final String SQL_DELETE_COMMENT =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
