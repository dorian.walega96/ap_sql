package com.example.ap_sql.utils;

import com.example.ap_sql.model.Post;
import com.example.ap_sql.model.Reaction;

import java.util.ArrayList;

public class TestModel {

    private Post post;
    private ArrayList<Reaction> reactions;
    private ArrayList<TestComment> comments;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public ArrayList<Reaction> getReactions() {
        return reactions;
    }

    public void setReactions(ArrayList<Reaction> reactions) {
        this.reactions = reactions;
    }

    public ArrayList<TestComment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<TestComment> comments) {
        this.comments = comments;
    }

}
