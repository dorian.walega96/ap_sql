package com.example.ap_sql.utils;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ap_sql.databinding.CommentViewBinding;
import com.example.ap_sql.model.CommentViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentViewHolder> {

    ArrayList<CommentViewModel> commentList;

    public CommentRecyclerViewAdapter(ArrayList<CommentViewModel> postArrayList) {
        this.commentList = postArrayList;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        CommentViewBinding binding = CommentViewBinding.inflate(layoutInflater, parent, false);
        return new CommentViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        CommentViewModel post = commentList.get(position);
        holder.bind(post);
    }

    @Override
    public int getItemCount() {
        return commentList != null ? commentList.size() : 0;
    }

    public ArrayList<CommentViewModel> getCommentList() {
        return commentList;
    }

    public void setCommentList(ArrayList<CommentViewModel> postList) {
        this.commentList = postList;
    }
}
