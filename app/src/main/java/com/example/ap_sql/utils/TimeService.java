package com.example.ap_sql.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class TimeService {

    private static long time;

    public static void startTimer() {
        time = System.nanoTime();
    }

    public static long stopTimer() {
        return System.nanoTime() - time;
    }

    public static void saveTimer(Context context, String description, long time) {
        try {
            File file = new File(context.getFilesDir(), "results.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            String msg = description + ", " + time + "ms\n";
            bufferedWriter.write(msg);
            Log.v("result", msg);
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

}
