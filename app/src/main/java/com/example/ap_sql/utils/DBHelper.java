package com.example.ap_sql.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ap_sql.model.Comment;
import com.example.ap_sql.model.CommentContract;
import com.example.ap_sql.model.CommentContract.CommentEntry;
import com.example.ap_sql.model.CommentReaction;
import com.example.ap_sql.model.CommentReactionContract.CommentReactionEntry;
import com.example.ap_sql.model.Post;
import com.example.ap_sql.model.PostContract.PostEntry;
import com.example.ap_sql.model.Reaction;
import com.example.ap_sql.model.ReactionContract.ReactionEntry;
import com.example.ap_sql.model.User;

import java.util.ArrayList;
import java.util.Date;

import static com.example.ap_sql.model.CommentContract.CommentEntry.SQL_CREATE_COMMENT;
import static com.example.ap_sql.model.CommentContract.CommentEntry.SQL_DELETE_COMMENT;
import static com.example.ap_sql.model.CommentReactionContract.CommentReactionEntry.SQL_CREATE_COMMENT_REACTION;
import static com.example.ap_sql.model.CommentReactionContract.CommentReactionEntry.SQL_DELETE_COMMENT_REACTION;
import static com.example.ap_sql.model.PostContract.PostEntry.SQL_CREATE_POST;
import static com.example.ap_sql.model.PostContract.PostEntry.SQL_DELETE_POST;
import static com.example.ap_sql.model.ReactionContract.ReactionEntry.SQL_CREATE_REACTION;
import static com.example.ap_sql.model.ReactionContract.ReactionEntry.SQL_DELETE_REACTION;
import static com.example.ap_sql.model.UserContract.UserEntry;
import static com.example.ap_sql.model.UserContract.UserEntry.SQL_CREATE_USER;
import static com.example.ap_sql.model.UserContract.UserEntry.SQL_DELETE_USER;


public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "King.db";
    private SQLiteDatabase db;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USER);
        db.execSQL(SQL_CREATE_POST);
        db.execSQL(SQL_CREATE_REACTION);
        db.execSQL(SQL_CREATE_COMMENT);
        db.execSQL(SQL_CREATE_COMMENT_REACTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_USER);
        db.execSQL(SQL_DELETE_POST);
        db.execSQL(SQL_DELETE_REACTION);
        db.execSQL(SQL_DELETE_COMMENT);
        db.execSQL(SQL_DELETE_COMMENT_REACTION);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean login(String login, String password) {
        String[] projection = {UserEntry.COLUMN_NAME_ID};

        String selection = UserEntry.COLUMN_NAME_LOGIN + " = ? AND " + UserEntry.COLUMN_NAME_PASSWORD + " = ?";
        String[] selectionArgs = {login, password};

        Cursor cursor = db.query(
                UserEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        ArrayList itemIds = new ArrayList<>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_ID));
            itemIds.add(itemId);
        }
        cursor.close();
        return !itemIds.isEmpty();
    }

    public long register(User newUser) {
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_LOGIN, newUser.getLogin());
        values.put(UserEntry.COLUMN_NAME_NAME, newUser.getName());
        values.put(UserEntry.COLUMN_NAME_NICKNAME, newUser.getNickname());
        values.put(UserEntry.COLUMN_NAME_PASSWORD, newUser.getPassword());
        return db.insert(UserEntry.TABLE_NAME, null, values);
    }

    public void addPost(Post post) {
        ContentValues values = new ContentValues();
        values.put(PostEntry.COLUMN_NAME_TEXT, post.getText());
        values.put(PostEntry.COLUMN_NAME_AUTHOR_ID, post.getAuthor().getId());
        values.put(PostEntry.COLUMN_NAME_DATE, post.getDate().toString());
        values.put(PostEntry.COLUMN_NAME_LOCATION, post.getLocation());
        db.insert(PostEntry.TABLE_NAME, null, values);
    }

    public ArrayList<Post> getPosts() {
        ArrayList<Post> posts = new ArrayList<>();

        String[] projection = {
                PostEntry.COLUMN_NAME_ID,
                PostEntry.COLUMN_NAME_TEXT,
                PostEntry.COLUMN_NAME_AUTHOR_ID,
                PostEntry.COLUMN_NAME_DATE,
                PostEntry.COLUMN_NAME_LOCATION};
        String sortOrder =
                PostEntry.COLUMN_NAME_DATE + " DESC";

        Cursor cursor = db.query(
                PostEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );
        while (cursor.moveToNext()) {
            Post post = new Post();
            post.setId(cursor.getInt(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_ID)));
            post.setText(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_TEXT)));
            post.setLocation(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_LOCATION)));
            post.setDate(new Date(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_DATE))));
            post.setAuthor(getUser(cursor.getInt(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_AUTHOR_ID))));
            posts.add(post);
        }

        return posts;
    }

    public Post getPost(int id) {
        String[] projection = {
                PostEntry.COLUMN_NAME_ID,
                PostEntry.COLUMN_NAME_TEXT,
                PostEntry.COLUMN_NAME_AUTHOR_ID,
                PostEntry.COLUMN_NAME_DATE,
                PostEntry.COLUMN_NAME_LOCATION};
        String selection = PostEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        Cursor cursor = db.query(
                PostEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor.moveToNext()) {
            Post post = new Post();
            post.setId(cursor.getInt(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_ID)));
            post.setText(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_TEXT)));
            post.setLocation(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_LOCATION)));
            post.setDate(new Date(cursor.getString(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_DATE))));
            post.setAuthor(getUser(cursor.getInt(cursor.getColumnIndexOrThrow(PostEntry.COLUMN_NAME_AUTHOR_ID))));
            return post;
        }
        return null;
    }

    public ArrayList<Reaction> getReactionsForPost(int id) {
        String[] projection = {
                ReactionEntry.COLUMN_NAME_ID,
                ReactionEntry.COLUMN_NAME_AUTHOR_ID,
                ReactionEntry.COLUMN_NAME_POST_ID};

        String selection = ReactionEntry.COLUMN_NAME_POST_ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        Cursor cursor = db.query(
                ReactionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        ArrayList<Reaction> reactions = new ArrayList<>();
        while (cursor.moveToNext()) {
            Reaction reaction = new Reaction();
            reaction.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ReactionEntry.COLUMN_NAME_ID)));
            reaction.setAuthorId(cursor.getInt(cursor.getColumnIndexOrThrow(ReactionEntry.COLUMN_NAME_AUTHOR_ID)));
            reaction.setPostId(cursor.getInt(cursor.getColumnIndexOrThrow(ReactionEntry.COLUMN_NAME_POST_ID)));
            reactions.add(reaction);
        }
        return reactions;
    }

    public ArrayList<CommentReaction> getReactionsForComment(int id) {
        String[] projection = {
                CommentReactionEntry.COLUMN_NAME_ID,
                CommentReactionEntry.COLUMN_NAME_AUTHOR_ID,
                CommentReactionEntry.COLUMN_NAME_COMMENT_ID};

        String selection = CommentReactionEntry.COLUMN_NAME_COMMENT_ID + " = ?";
        String[] selectionArgs = { String.valueOf(id) };

        Cursor cursor = db.query(
                CommentReactionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        ArrayList<CommentReaction> reactions = new ArrayList<>();
        while (cursor.moveToNext()) {
            CommentReaction reaction = new CommentReaction();
            reaction.setId(cursor.getInt(cursor.getColumnIndexOrThrow(CommentReactionEntry.COLUMN_NAME_ID)));
            reaction.setAuthorId(cursor.getInt(cursor.getColumnIndexOrThrow(CommentReactionEntry.COLUMN_NAME_AUTHOR_ID)));
            reaction.setCommentId(cursor.getInt(cursor.getColumnIndexOrThrow(CommentReactionEntry.COLUMN_NAME_COMMENT_ID)));
            reactions.add(reaction);
        }
        return reactions;
    }

    public User getUser(int id) {
        String[] projection = {
                UserEntry.COLUMN_NAME_ID,
                UserEntry.COLUMN_NAME_LOGIN,
                UserEntry.COLUMN_NAME_NICKNAME,
                UserEntry.COLUMN_NAME_NAME};

        String selection = UserEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};
        Cursor cursor = db.query(
                UserEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor.moveToNext()) {
            User user = new User();
            user.setId(cursor.getInt(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_ID)));
            user.setLogin(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_LOGIN)));
            user.setName(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_NAME)));
            user.setNickname(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_NICKNAME)));
            return user;
        }
        return null;
    }

    public User getUser(String login) {
        String[] projection = {
                UserEntry.COLUMN_NAME_ID,
                UserEntry.COLUMN_NAME_LOGIN,
                UserEntry.COLUMN_NAME_NICKNAME,
                UserEntry.COLUMN_NAME_NAME};

        String selection = UserEntry.COLUMN_NAME_LOGIN + " = ?";
        String[] selectionArgs = {login};
        Cursor cursor = db.query(
                UserEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor.moveToNext()) {
            User user = new User();
            user.setId(cursor.getInt(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_ID)));
            user.setLogin(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_LOGIN)));
            user.setName(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_NAME)));
            user.setNickname(cursor.getString(cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_NICKNAME)));
            return user;
        }
        return null;
    }

    public void postReact(Post post, User user) {
        String[] projection = {
                ReactionEntry.COLUMN_NAME_ID,
                ReactionEntry.COLUMN_NAME_POST_ID,
                ReactionEntry.COLUMN_NAME_AUTHOR_ID};

        String selection = ReactionEntry.COLUMN_NAME_POST_ID + " = ? AND " + ReactionEntry.COLUMN_NAME_AUTHOR_ID + " = ?";
        String[] selectionArgs = {String.valueOf(post.getId()), String.valueOf(user.getId())};

        Cursor cursor = db.query(
                ReactionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor.moveToNext()) {
            deleteReaction(cursor.getInt(cursor.getColumnIndexOrThrow(ReactionEntry.COLUMN_NAME_ID)));
        } else {
            addReaction(post.getId(), user.getId());
        }
    }

    public void deleteReaction(int reactionId) {
        String selection = ReactionEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(reactionId) };
        int deletedRows = db.delete(ReactionEntry.TABLE_NAME, selection, selectionArgs);
    }

    private void addReaction(int postId, int userId) {
        ContentValues values = new ContentValues();
        values.put(ReactionEntry.COLUMN_NAME_POST_ID, postId);
        values.put(ReactionEntry.COLUMN_NAME_AUTHOR_ID, userId);
        db.insert(ReactionEntry.TABLE_NAME, null, values);
    }

    public void commentReact(Comment comment, User user) {
        String[] projection = {
                CommentReactionEntry.COLUMN_NAME_ID,
                CommentReactionEntry.COLUMN_NAME_COMMENT_ID,
                CommentReactionEntry.COLUMN_NAME_AUTHOR_ID};

        String selection = CommentReactionEntry.COLUMN_NAME_COMMENT_ID + " = ? AND " + ReactionEntry.COLUMN_NAME_AUTHOR_ID + " = ?";
        String[] selectionArgs = {String.valueOf(comment.getId()), String.valueOf(user.getId())};

        Cursor cursor = db.query(
                CommentReactionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor.moveToNext()) {
            deleteCommentReaction(cursor.getInt(cursor.getColumnIndexOrThrow(ReactionEntry.COLUMN_NAME_ID)));
        } else {
            addCommentReaction(comment.getId(), user.getId());
        }
    }

    public void deleteCommentReaction(int reactionId) {
        String selection = CommentReactionEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(reactionId) };
        int deletedRows = db.delete(CommentReactionEntry.TABLE_NAME, selection, selectionArgs);
    }

    private void addCommentReaction(int commentId, int userId) {
        ContentValues values = new ContentValues();
        values.put(CommentReactionEntry.COLUMN_NAME_COMMENT_ID, commentId);
        values.put(CommentReactionEntry.COLUMN_NAME_AUTHOR_ID, userId);
        db.insert(CommentReactionEntry.TABLE_NAME, null, values);
    }

    public void addComment(Comment comment) {
        ContentValues values = new ContentValues();
        values.put(CommentEntry.COLUMN_NAME_POST_ID, comment.getPost().getId());
        values.put(CommentEntry.COLUMN_NAME_AUTHOR_ID, comment.getAuthor().getId());
        values.put(CommentEntry.COLUMN_NAME_DATE, comment.getDate().toString());
        values.put(CommentEntry.COLUMN_NAME_TEXT, comment.getText());
        db.insert(CommentEntry.TABLE_NAME, null, values);
    }

    public ArrayList<Comment> getComments(int postId) {
        String[] projection = {
                CommentEntry.COLUMN_NAME_ID,
                CommentEntry.COLUMN_NAME_AUTHOR_ID,
                CommentEntry.COLUMN_NAME_POST_ID,
                CommentEntry.COLUMN_NAME_TEXT,
                CommentEntry.COLUMN_NAME_DATE};

        String selection = CommentEntry.COLUMN_NAME_POST_ID + " = ?";
        String[] selectionArgs = { String.valueOf(postId) };
        String sortOrder = CommentEntry.COLUMN_NAME_DATE + " DESC";

        Cursor cursor = db.query(
                CommentEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        ArrayList<Comment> comments = new ArrayList<>();
        while (cursor.moveToNext()) {
            Comment comment = new Comment();
            comment.setId(cursor.getInt(cursor.getColumnIndexOrThrow(CommentEntry.COLUMN_NAME_ID)));
            comment.setText(cursor.getString(cursor.getColumnIndexOrThrow(CommentEntry.COLUMN_NAME_TEXT)));
            comment.setDate(new Date(cursor.getString(cursor.getColumnIndexOrThrow(CommentEntry.COLUMN_NAME_DATE))));
            comment.setAuthor(getUser(cursor.getInt(cursor.getColumnIndexOrThrow(CommentEntry.COLUMN_NAME_AUTHOR_ID))));
            comments.add(comment);
        }
        return comments;
    }

    public int getLastIndex(String tableName){
        String selectQuery = "SELECT  MAX(ID) AS MAX_ID FROM " + tableName;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToNext()) {
            return c.getInt(0);
        }
        return -1;
    }

    public void deleteComment(int reactionId) {
        String selection = CommentEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(reactionId) };
        int deletedRows = db.delete(CommentEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void deletePost(int reactionId) {
        String selection = PostEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(reactionId) };
        int deletedRows = db.delete(PostEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void deleteUser(int reactionId) {
        String selection = UserEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = { String.valueOf(reactionId) };
        int deletedRows = db.delete(UserEntry.TABLE_NAME, selection, selectionArgs);
    }
}
