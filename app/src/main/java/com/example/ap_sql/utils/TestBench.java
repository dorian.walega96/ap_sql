package com.example.ap_sql.utils;

import android.content.Context;

import com.example.ap_sql.model.Comment;
import com.example.ap_sql.model.CommentReactionContract;
import com.example.ap_sql.model.Post;
import com.example.ap_sql.model.User;
import com.example.ap_sql.model.CommentContract.CommentEntry;
import com.example.ap_sql.model.CommentReactionContract.CommentReactionEntry;
import com.example.ap_sql.model.PostContract.PostEntry;
import com.example.ap_sql.model.ReactionContract.ReactionEntry;
import com.example.ap_sql.model.UserContract.UserEntry;

import java.util.ArrayList;
import java.util.Date;

public class TestBench {

    private DBHelper db;

    public TestBench(Context context) {
        db = new DBHelper(context);
    }

    public void testInsert(int count) {
        for (int i = 0; i < count; i++) {
            String login = "user" + i;
            String name = "King " + i;
            User user = new User(-1, login, name, name, "user");
            db.register(user);

            Post post = new Post(-1, "Treść posta nie za długa, nie za krótka, taka w sam raz.", user, new Date(), "TestBench");
            db.addPost(post);

            db.postReact(post, user);

            Comment comment = new Comment(-1, post, "Treść komentarza, nie za długa, nie za krótka, taka w sam raz.", user, new Date());
            db.addComment(comment);

            db.commentReact(comment, user);
        }
    }

    public void testSelect(int count) {
        ArrayList<Post> posts = db.getPosts();
        ArrayList<TestModel> testModels = new ArrayList<>(count);
        posts.forEach(post -> {
            TestModel testModel = new TestModel();
            testModel.setPost(post);

            testModel.setReactions(db.getReactionsForPost(post.getId()));

            ArrayList<Comment> comments = db.getComments(post.getId());

            ArrayList<TestComment> testComments = new ArrayList<>(comments.size());
            comments.forEach(comment -> {
                TestComment testComment = new TestComment();
                testComment.setComment(comment);

                //TODO commentreactions
                testComment.setCommentReactions(db.getReactionsForComment(comment.getId()));

                testComments.add(testComment);
            });
            testModel.setComments(testComments);
            testModels.add(testModel);
        });
    }

    public void testDelete(int count) {
        for (int i = 0; i < count; i++) {
            db.deleteCommentReaction(db.getLastIndex(CommentReactionEntry.TABLE_NAME));
            db.deleteComment(db.getLastIndex(CommentEntry.TABLE_NAME));
            db.deleteReaction(db.getLastIndex(ReactionEntry.TABLE_NAME));
            db.deletePost(db.getLastIndex(PostEntry.TABLE_NAME));
            db.deleteUser(db.getLastIndex(UserEntry.TABLE_NAME));
        }
    }

}
