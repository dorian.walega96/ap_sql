package com.example.ap_sql.utils;

import com.example.ap_sql.databinding.CommentViewBinding;
import com.example.ap_sql.model.CommentViewModel;

import androidx.recyclerview.widget.RecyclerView;

public class CommentViewHolder extends RecyclerView.ViewHolder {

    private CommentViewBinding binding;

    public CommentViewHolder(CommentViewBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(CommentViewModel item) {
        binding.setComment(item);
        binding.like.setOnClickListener(view -> reaction());
        binding.executePendingBindings();
    }

    private void reaction() {
        // ((PostActivity) this.itemView.getContext()).reaction(binding.getPost());
    }
}
