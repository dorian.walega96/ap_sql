package com.example.ap_sql.utils;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ap_sql.databinding.PostViewBinding;
import com.example.ap_sql.model.PostViewModel;

import java.util.ArrayList;

public class PostRecyclerViewAdapter extends RecyclerView.Adapter<PostViewHolder> {

    ArrayList<PostViewModel> postList;

    public PostRecyclerViewAdapter(ArrayList<PostViewModel> postArrayList) {
        this.postList = postArrayList;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        PostViewBinding binding = PostViewBinding.inflate(layoutInflater, parent, false);
        return new PostViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        PostViewModel post = postList.get(position);
        holder.bind(post);
    }

    @Override
    public int getItemCount() {
        return postList != null ? postList.size() : 0;
    }

    public ArrayList<PostViewModel> getPostList() {
        return postList;
    }

    public void setPostList(ArrayList<PostViewModel> postList) {
        this.postList = postList;
    }
}
