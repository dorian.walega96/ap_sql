package com.example.ap_sql.utils;

import com.example.ap_sql.model.Comment;
import com.example.ap_sql.model.CommentReaction;

import java.util.ArrayList;

public class TestComment {
    private Comment comment;
    private ArrayList<CommentReaction> commentReactions;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public ArrayList<CommentReaction> getCommentReactions() {
        return commentReactions;
    }

    public void setCommentReactions(ArrayList<CommentReaction> commentReactions) {
        this.commentReactions = commentReactions;
    }
}